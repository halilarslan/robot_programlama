/*
Atahan Duman - 2013123043
*/

#include <iostream>

using namespace std;

int *malzemeAl(int TCP[],int malzeme_konum[]);
int *malzemeBirak(int TCP[],int stok_konum[]);
int *XkonumDegis(int stok_konum[],int x);
int *YkonumDegis(int stok_konum[],int y);
int sayac=0,mesafe=0;
int *Yuk;

int main()
{
  int malzeme_konum[] = {10,10,10};
  int TCP[]= {5,5,5};
  int stok_konum[3] = {25,25,25};
  
  cout<<"Stok diziliminde malzemeler arasi kac santim olarak ayarlansin ? "<<endl;
  cin>>mesafe;
  
  while(1)
  {
  	for(int i=0;i<3;i++)
  	{
  		for(int j=0;j<3;j++)
  		{
  			malzemeAl(TCP,malzeme_konum);
  			malzemeBirak(TCP,stok_konum);
  			YkonumDegis(stok_konum,mesafe);
		}
		XkonumDegis(stok_konum,mesafe);
	}
	cout<<" \nYuk transfer islemi basariyla tamamlandi !";
  	break;
  }
  
  return 0;
}

int *malzemeAl(int TCP[],int malzeme_konum[]){
  
  	for (int i=0;i<3;i++)
  	{
  		TCP[i]=malzeme_konum[i];
	}
	sayac++;
	cout<<sayac<<". Malzeme alindi."<<endl;
	
	return TCP;
}

int *malzemeBirak(int TCP[],int stok_konum[])
{
	for(int i=0;i<3;i++)
	{
		TCP[i]=stok_konum[i];
		
	}
	cout<<sayac<<". Stok alanina malzeme yerlestirildi."<<endl;
	return TCP;
}

int *XkonumDegis(int stok_konum[],int x)
{
	stok_konum[0]+=x;
	stok_konum[1]=25;
	return stok_konum;
}

int *YkonumDegis(int stok_konum[],int y)
{
	stok_konum[1]+=y;
	return stok_konum;
}


