#include <stdio.h>
#include <conio.h>


void moveBase(int x, int y, int z)
{
	printf("\nRobot : x=%d y=%d z=%d base konumunda\n",x,y,z);
}


void moveTool(int s1, int s2 , int s3)
{
	printf("Robot : x=%d y=%d z=%d tool konumunda\n",s1,s2,s3);
}


void sleep(){
	printf("stop");
}


int main ()
{
	int tool[3]={ 15, 20, 17 };
	int base[3]={ 20, 40, 60};
	
	while(true)
	{
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				moveTool(tool[j] , tool[j] , tool[j]);
				moveBase(base[j] - (10*i) , base[j] + (10*j) , base[j]);
			}
		}
		sleep();
		break;
	}	
}
